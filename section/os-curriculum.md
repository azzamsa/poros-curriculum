# OS Section Curriculum

## Learn

### Programming
- Fundamental Programming
- Pyhton

### Tools

#### Text Editor
- Emacs / Vi

#### Version Control
- Git

#### Documents
- Latex
- Markdown

#### Unix
- Awk
- Shell scripting

#### Linux
- simple kernel module

#### Misc
- Tmux

### Advanced Programming
- Common Lisp
- Haskell
- OCaml


### Leisure

#### Window Manager
- anything you like!


## Output

### Contribution
- Code Contributon
- Translation Contribution

### Quizez
- Create CV, docs using LaTeX
- Solve problem using pyhton
- do the jobs in emacs/vi



## Reference

- [What every computer science major should know ](http://matt.might.net/articles/what-cs-majors-should-know/)
- [Path to a free self-taught education in Computer Science!](https://github.com/ossu/computer-science)
- [Teach Yourself Computer Science](https://teachyourselfcs.com/)
- OS
  - [mikeOS](http://mikeos.sourceforge.net/write-your-own-os.html)
    - http://mikeos.sourceforge.net/write-your-own-os.html
  - https://littleosbook.github.io/
  - [Writing a Simple Operating System from Scrath](http://www.cs.bham.ac.uk/%7Eexr/lectures/opsys/10_11/lectures/os-dev.pdf)
  - [KernelNewbies](http://kernelnewbies.org/)
  - [OSDev.org](https://wiki.osdev.org/Main_Page
  - [os-tutorial](https://github.com/cfenollosa/os-tutorial)
